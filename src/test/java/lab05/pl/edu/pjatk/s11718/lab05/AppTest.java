package lab05.pl.edu.pjatk.s11718.lab05;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;


public class AppTest {

	private static WebDriver driver;
	WebElement element;

	@BeforeClass
	public static void driverSetup() {
		System.setProperty("webdriver.chrome.driver", "F:/testowanie/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test
	public void homePage(){
		driver.get("http://demoqa.com/");
		
		element = driver.findElement(By.linkText("Registration"));
		assertNotNull(element);
	}
	
	@Test
	public void registerPage(){
		driver.get("http://demoqa.com/");
		driver.findElement(By.linkText("Registration")).click();
		element = driver.findElement(By.linkText("Registration"));
		WebElement first_name = driver.findElement(By.name("first_name"));
		first_name.sendKeys("Damian");
		WebElement last_name = driver.findElement(By.name("last_name"));
		last_name.sendKeys("Gackowski");
		WebElement phone_9 = driver.findElement(By.name("phone_9"));
		phone_9.sendKeys("48123123123");
		WebElement username = driver.findElement(By.name("username"));
		username.sendKeys("Drizzt1");
		WebElement e_mail = driver.findElement(By.name("e_mail"));
		e_mail.sendKeys("s11718@pjwstk.edu.pl");
		WebElement description = driver.findElement(By.name("description"));
		description.sendKeys("Somethink");
		WebElement password = driver.findElement(By.name("password"));
		password.sendKeys("hasło123!");
		WebElement confirm_password_password_2 = driver.findElement(By.id("confirm_password_password_2"));
		confirm_password_password_2.sendKeys("hasło123!");
		new Select(driver.findElement(By.name("dropdown_7"))).selectByVisibleText("Poland");
		new Select(driver.findElement(By.id("mm_date_8"))).selectByVisibleText("3");
		new Select(driver.findElement(By.id("dd_date_8"))).selectByVisibleText("25");
		new Select(driver.findElement(By.id("yy_date_8"))).selectByVisibleText("1993");
		driver.findElement(By.cssSelector("input[value='single']")).click();
		driver.findElement(By.cssSelector("input[value='reading']")).click();
		
		
	    
		
		driver.findElement(By.name("pie_submit")).click();
		assertTrue(driver.findElement(By.tagName("body")).getText().contains("E-mail address already exists"));
	   
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
	    jse.executeScript("scroll(0, -250);");
	    
	}
		
	@Test
	public void dragAndDrop(){
		driver.get("http://demoqa.com/droppable/");
		
		element = driver.findElement(By.xpath("//*[contains(text(),'" + "Drag me" + "')]")); 
		WebElement target = driver.findElement(By.xpath("//*[contains(text(),'" + "Drop here" + "')]"));
		(new Actions(driver)).dragAndDrop(element, target).perform();
		String actualText=driver.findElement(By.cssSelector("#droppableview>p")).getText();
		Assert.assertEquals(actualText, "Dropped!");
		
	}
	

	@AfterClass
	public static void cleanp() {
		driver.quit();
	}
}
